# Package for training a classifier for analysis. 

This package allows for using advanced ML methods to discriminate signal and background in ATLAS physics analyses. Currently only XGBoost model training is implemented on high level variables. 

## Installation
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS -q
lsetup "root 6.10.04-x86_64-slc6-gcc62-opt"

- Install a local version of pip:
wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py --user

- Add the location of the pip install into your path:
export PATH=$HOME/.local/bin:$PATH

- If you are using setupATLAS on you will need to change
~/.local/bin/pip. I recommended changing the long ugly first line (with the python executable location) to "!/usr/bin/env python"

- Install a local version of ROOT and source the setup script (not necessary when using setupATLAS):
source /projects/chep/whopkins/root/bin/thisroot.sh

- Update six and numpy which is a module required by matplotlib:
pip install matplotlib keras tensorflow numpy xgboost root_numpy sklearn --user --upgrade

Here is a description of why you need these packages:
- root_numpy -> useful for converting root trees to numpy arrays. This allows you to use not just sobel transforms but any other transform in the scipy.ndimage package. You can also then use Keras or XGBoost.
- h5py -> allows you to save your numpy arrays in the HDF format.
- matplotlib -> allows you to plot your numpy arrays.
- sklearn -> has various tools for ML such as splitting datasets into testing and training sets. .

## Setup
Before you start from a new terminal source `setup.sh`. This should work without any additional hacks on talapas. For lxplus, spar, acas, or hepatl simply use the first three commands. 

## Algorithm description.

Coming soon...


## Future features
- Fix bug in response. LSTM and BDT get same accuracy but very different response
- Test multiclass with keras models
- Add history callbacks for keras
- Add CNNs using jets in a grid with a particular granularity (0.2x0.2 in eta x phi)
- Add calorimeter cell information. This will require lots of work in dealing with massive amount of information.
