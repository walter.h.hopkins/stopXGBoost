#!/usr/bin/env python
from keras.models import Sequential, load_model, Model
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, Input
from keras.layers.merge import Concatenate
from keras.layers.advanced_activations import LeakyReLU, PReLU
from keras.layers import LSTM, Flatten, GRU
import numpy as np
from keras import initializers

def moduleExists(moduleName='xgboost'):
    try:
        __import__(moduleName)
    except ImportError:
        return False
    else:
        return True

gotXGBoost = moduleExists()

def model_MLP_1(inputShape, savePath):
    model = Sequential()
    model.add(BatchNormalization(input_shape=inputShape))

    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())

    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(256, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(512, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(256, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))
    model.add(Dense(64, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.3))

    model.add(Dense(1, activation='sigmoid'))
    
    # Set loss and optimizer
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',])
    
    # Store model to file
    model.save(savePath)
    return model

def model_conv1D(inputShape, savePath):
    model = Sequential()
    model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    model.add(Dense(1))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    model.save(savePath)
    return model

def model_lstm(inputShape, savePath, metrics):
    model = Sequential()
    #model.add(BatchNormalization(input_shape=inputShape))
    #model.add(LSTM(1000))    
    model.add(LSTM(100,input_shape=inputShape))    
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=metrics)
    model.save(savePath)
    return model

def model_GRU(inputShape, savePath, metrics):
    model = Sequential()
    #model.add(BatchNormalization(input_shape=inputShape))
    model.add(GRU(1000,input_shape=inputShape))    
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=metrics)
    model.save(savePath)
    return model

def model_lstm_deeper(inputShape, savePath, metrics):
    model = Sequential()
    #model.add(BatchNormalization(input_shape=inputShape))
    #model.add(LSTM(1000))    
    model.add(LSTM(100,input_shape=inputShape, return_sequences=True))
    model.add(LSTM(100, return_sequences=True))    
    model.add(LSTM(100))    
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=metrics)
    model.save(savePath)
    return model

if gotXGBoost:
    import xgboost
    # xgboost models can't be saved until they are trained.
    def model_xgboost(featureNames):
        nTrees = 1000;
        learningRate = 0.3
        subsample = 0.6
        nJobs = 16
        silent = True

        model = xgboost.XGBClassifier(n_estimators=nTrees, learning_rate=learningRate, subsample=subsample, n_jobs=nJobs, silent=silent, objective='binary:logistic', feature_names=featureNames)
        return model

    def model_xgboostMulticlass(featureNames):
        nTrees = 1000;
        learningRate = 0.3
        subsample = 0.6
        nJobs = 16
        silent = True

        model = xgboost.XGBClassifier(n_estimators=nTrees, learning_rate=learningRate, subsample=subsample, n_jobs=nJobs, silent=silent, objective='binary:logistic', feature_names=featureNames)
        return model

        

