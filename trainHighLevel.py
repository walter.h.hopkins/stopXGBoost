#!/usr/bin/env python
"""@package docstring
This module preps the data and trains and tests various MVAs using high level inputs such as Mt, Met, top candidate mass, nJets, etc.
"""

import sys, os, argparse, matplotlib, cutStrings, pickle
import numpy as np
np.random.seed(19)

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping

from keras.models import load_model

from MLUtils import *

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)

print 'Available models'
print '\n'.join(sorted(modelDict.keys()))

from sklearn.metrics import accuracy_score

# Branches used for training
branchSets = {
    "stopVars":["Met", ("AntiKt12M[0]",0,1), ("AntiKt12M[1]",0,1), ("AntiKt8M[0]",0,1), "MtBMin", "MtBMax", "DRBB"],
    #"stopVarsReduced":[("AntiKt12M[0]",0,1), ("AntiKt8M[0]",0,1), "MtBMin", "MtBMax", "DRBB"],
    "stopVarsReduced":[("AntiKt12M[0]",0,1), "MtBMin", "MtBMax", "DRBB"],
    "stopFourVecBTag":["JetPt", "JetIsBTagged", "JetEta", "JetPhi"],
    "stopFourVec":["JetPt", "JetEta", "JetPhi"],
    "stopJetPt":["JetPt"],
}

prepFuncs = {
    "stopVars":prepROOTDataML1D,
    "stopVarsReduced":prepROOTDataML1D,
    "stopVarsReducedNoR8Mass":prepROOTDataML1D,
    "stopFourVecBTag":prepROOTDataMLFourVecs,
    "stopFourVec":prepROOTDataMLFourVecs,
    "stopJetPt":prepROOTDataMLFourVecs,
}
# Set of branches to also grab for comaprisons. The branch name in a tuple (if accessing a vector) or string comes first. Then the scaling, binning and units.
extraBranchSets = {
    "stopVars":[
        ('AntiKt12M[0]',0,1),
        ('AntiKt12M[1]',0,1),
        ('AntiKt8M[0]',0,1),
        ('AntiKt8M[1]',0,1),
        "MtBMin",
        "MtBMax",
        "NBJets",
        "NJets",
        "Met",
        "DRBB",
        "MtTauCand",
    ],
    "stopVarsReduced":[
        ('AntiKt12M[0]',0,1),
        ('AntiKt12M[1]',0,1),
        ('AntiKt8M[0]',0,1),
        ('AntiKt8M[1]',0,1),
        "MtBMin",
        "MtBMax",
        "NBJets",
        "NJets",
        "Met",
        "DRBB",
        "MtTauCand",
    ],
    "stopFourVec":[
        ('AntiKt12M[0]',0,1),
        ('AntiKt12M[1]',0,1),
        ('AntiKt8M[0]',0,1),
        ('AntiKt8M[1]',0,1),
        "MtBMin",
        "MtBMax",
        "NBJets",
        "NJets",
        "Met",
        "DRBB",
        "MtTauCand",
    ],
}

binning = {
    'AntiKt12M[0]':([i*20 for i in range(21)], 1., 'GeV'),
    'AntiKt12M[1]':([i*20 for i in range(16)], 1., 'GeV'), 
    'AntiKt8M[0]':([i*20 for i in range(16)], 1., 'GeV'), 
    'AntiKt8M[1]':([i*20 for i in range(16)], 1., 'GeV'), 
    "MtBMin":([i*50 for i in range(17)], 1., 'GeV'), 
    "MtBMax":([i*50 for i in range(17)], 1., 'GeV'), 
    "NBJets":([i for i in range(6)], 1., ''), 
    "NJets":([i for i in range(21)], 1., ''), 
    "Met":([200+i*50 for i in range(11)], 1., 'GeV'), 
    "DRBB":([i*0.4 for i in range(12)], 1., ''), 
    "JetIsBTagged":([i for i in range(2)], 1., ''), 
    "JetEta":([-4.6+i*0.2 for i in range(47)], 1., ''), 
    "JetPhi":([-3.2+i*0.2 for i in range(31)], 1., ''),
    'JetPt':([i*20 for i in range(16)], 1., 'GeV'), 
    "MtTauCand":([i*10 for i in range(21)], 1., 'GeV'),
    }


def train(modelNames, modelFPath, h5Path, sigFName, bkgFNames, epochs=20, batchSize=20, nTrainSamp=20000, nTestSamp=1000, precutKey='NoCut', sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', branchSetKey='stopVars', extraBranchKey='', weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], suffix="", shortSigName='signal', shortBkgNames=['bkg'], updateSamples=False, lumi=36, multiclass=True, metric='acc'):

    featureNames = []
    for branch in branchSets[branchSetKey]:
        if type(branch)==tuple:
            featureNames.append(branch[0])
        else:
            featureNames.append(branch)
            
    branches = []
    branches+=sorted(branchSets[branchSetKey])
    if extraBranchKey != '':
        branches+=[branchName for branchName in sorted(extraBranchSets[extraBranchKey]) if branchName not in branches]
    testTrainInfo = prepFuncs[branchSetKey](nTrainSamp, nTestSamp, branches, branchSetKey, len(branchSets[branchSetKey]), h5Path,
                                            sigFName, bkgFNames, sigTreeName=sigTreeName, bkgTreeName=bkgTreeName,
                                            sigFriendName=sigFriendName, bkgFriendName=bkgFriendName, precutKey=precutKey,
                                            weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"],
                                            lumi=lumi, update=updateSamples, shortSigName=shortSigName, shortBkgNames=shortBkgNames)

    xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest = testTrainInfo[:6]
    extraVarsTrain, extraVarsTest = testTrainInfo[6:8]
    sigGrid, bkgGrid = testTrainInfo[8:10]
    sigExtraVars, bkgExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset = testTrainInfo[10:-3]
    sigWeights, bkgWeights = testTrainInfo[-3:-1]
    multiclassInfo = testTrainInfo[-1]
    print "Training model. Multiclass is set to", multiclass
    for modelName in modelNames:
        savePath = modelFPath+branchSetKey+'_'+precutKey+'_'+modelName+'.h5'
        if 'xgboost' in modelName:
            model = modelDict[modelName](featureNames)
        else:
            print 'boohoo', xTrain.shape[1:]
            model = modelDict[modelName](xTrain.shape[1:], savePath, [metric])
            
        if multiclass:
            (xTrain, yTrain, xTest, yTest, weightsTrain) = multiclassInfo[:5]
            if 'Multiclass' not in modelName:
                print 'You can\'t use multiclass if you aren\'t using a multiclass model.'

        if 'xgboost' in modelName:
            eval_set = [(xTrain, yTrain), (xTest, yTest)]
            model.fit(xTrain, yTrain, sample_weight=weightsTrain, eval_metric=["error", "logloss"], eval_set=eval_set, early_stopping_rounds=50, verbose=False)
        else:
            earlystop = EarlyStopping(monitor='val_'+metric, min_delta=0.01, patience=5, verbose=1, mode='auto')
            callbacks_list = [earlystop]
            history = model.fit(xTrain, yTrain, epochs=epochs, batch_size=batchSize, shuffle=False, sample_weight=weightsTrain, validation_data=(xTest,yTest,weightsTest))
            fig, ax = plt.subplots()

            plt.plot(history.history[metric])
            plt.plot(history.history['val_'+metric])
            ax.set_xlabel('epoch')
            ax.set_ylabel(metric)
            leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
            plt.savefig(metric+'_'+modelName+'.pdf', bbox_inches='tight')

            plt.clf()

            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            ax.set_xlabel('epoch')
            ax.set_ylabel('Loss')
            leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
            plt.savefig('loss_'+modelName+'.pdf', bbox_inches='tight')

        yPred = model.predict(xTest)
        predictions = [round(value) for value in yPred]
        accuracy = accuracy_score(yTest, predictions)
        print("Accuracy: %.2f%%" % (accuracy * 100.0))

        if 'xgboost' in modelName:
            pickle.dump(model, open(savePath, 'wb'))

        
def testModel(h5Path, sigFName, bkgFNames, modelFPath, modelNames, nTrainSamp=10000, nTestSamp=10000, precutKey='NoCut', compRegions=['SRB'], sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', branchSetKey='', extraBranchKey='', weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], shortSigName='signal', shortBkgNames=['bkg'], updateResponse=False, dB=0.15, lumi=36, multiclass=True):
    """! This function produces various figures of merits for the trained model such as training and testing output, trained weights,
    and turn-on curves."""
    branches = []
    branches+=sorted(branchSets[branchSetKey])
    if extraBranchKey != '':
        branches+=[branchName for branchName in sorted(extraBranchSets[extraBranchKey]) if branchName not in branches]
    testTrainInfo = prepFuncs[branchSetKey](nTrainSamp, nTestSamp, branches, branchSetKey, len(branchSets[branchSetKey]), h5Path, sigFName, bkgFNames,
                                            sigTreeName=sigTreeName, bkgTreeName=bkgTreeName, sigFriendName=sigFriendName,
                                            bkgFriendName=bkgFriendName, precutKey=precutKey,
                                            weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"],
                                            lumi=lumi, shortSigName=shortSigName, shortBkgNames=shortBkgNames)
    
    testTrainInfoMulticlass = testTrainInfo[-1]
    testTrainInfo = testTrainInfo[:-1]

    featureNames = []
    for branch in sorted(branchSets[branchSetKey]):
        if type(branch)==tuple:
            featureNames.append(branch[0])
        else:
            featureNames.append(branch)
    yieldInfo = {}
    weightStr = "("+"*".join(weightBranches)+"*1000*"+str(lumi)+")"
    for compRegion in compRegions:
        compRegionCut = "("+cutStrings.allCuts[compRegion]+")*("+weightStr+")"
        yieldInfo[compRegion] = getSigBkgEffFromCut(compRegionCut, sigFName, bkgFNames,
                                                    precutKey="("+cutStrings.allCuts[precutKey]+")*("+weightStr+")",
                                                    update=updateResponse, suffix=shortSigName+'_'+'_'.join(shortBkgNames))

    extraVarsTrain,extraVarsTest = testTrainInfo[6:8]
    allData = testTrainInfo[8:10]
    extraVars = testTrainInfo[10:12]
    allWeights = testTrainInfo[-2:]
    # Append the summed background to the test/train info
    testTrainData = []
    if len(shortBkgNames) > 1:
        for i in range(4):
            totalBkgClass = 1;
            if i==3 or i==1:
                totalBkgClass=len(shortBkgNames)+1
            testTrainData.append(np.concatenate((testTrainInfoMulticlass[i], totalBkgClass*testTrainInfo[i]), axis=0))
        testTrainWeights = [np.concatenate((testTrainInfoMulticlass[i], testTrainInfo[i]),axis=0) for i in range(4,6)]
        allData = [allData[0], testTrainInfoMulticlass[6]+[allData[1]]]
        allWeights = [allWeights[0], testTrainInfoMulticlass[7]+[allWeights[1]]]
    else:
        testTrainData = testTrainInfoMulticlass[:4]
        testTrainWeights = [testTrainInfoMulticlass[i] for i in range(4,6)]
        allData = [allData[0], testTrainInfoMulticlass[6]]
        allWeights = [allWeights[0], testTrainInfoMulticlass[7]]
    sigBkgEffs= {}
    models = {}
    newShortBkgNames = shortBkgNames
    if len(shortBkgNames) > 1:
        newShortBkgNames = shortBkgNames+['totalBkg']

    # Add the sum of the backgrounds and fix the preselection to only use the specified yield for a particular background.
    sigPrecutYield = yieldInfo.values()[0][0];
    bkgTotalPrecutYield = yieldInfo.values()[0][1];

    bkgPrecutYield = []
    for bkgI in range(len(bkgFNames)):
        bkgPrecutYield.append(yieldInfo.values()[0][2][bkgFNames[bkgI]])

    if len(shortBkgNames) > 1:
        bkgPrecutYield.append(bkgTotalPrecutYield)
        for region in yieldInfo:
            yieldInfo[region][8].append(yieldInfo[region][7])

    allResponses = {}
    testTrainResponses = {}
    for modelName in modelNames:
        modelFile = modelFPath+branchSetKey+'_'+precutKey+'_'+modelName+'.h5'
        print "Fetching", modelName

        if 'xgboost' in modelName:
            model = pickle.load(open(modelFile, "rb"))
        else:
            model = load_model(modelFile)
            model.summary()

        if 'xgboost' in modelName:
            analyzeTrees(model, featureNames, binning, suffix=modelName)
            
        models[modelName] = model
        
        print "Getting signal and background efficiency for", modelName
        responseFName = 'response_'+branchSetKey+'_'+precutKey+'_'+modelName+'_'+shortSigName+'_'.join(shortBkgNames)+'.pkl'
        (responses, sigBkgEffs[modelName]) = getSigBkgEffFromModel(model, testTrainData, testTrainWeights, allData, allWeights, responseFName, binning=[i*.002 for i in range(501)], update=updateResponse, xgb='xgboost' in modelName, multiclass=multiclass)
        
        testTrainResponses[modelName] = responses[:4]
        allResponses[modelName] = responses[4:]
        yTrain = testTrainData[1]
        yTest = testTrainData[3]
        [xTrainW, xTestW] = testTrainWeights
        sigTestW = xTestW[yTest==0]
        sigTrainW = xTrainW[yTrain==0]
        bkgTestW = []
        bkgTrainW = []
        for classI in range(1, np.unique(yTest).shape[0]):
            bkgTestW.append(xTestW[yTest==classI])
            bkgTrainW.append(xTrainW[yTrain==classI])
        sigBkgTestTrainW = (sigTrainW, bkgTrainW, sigTestW, bkgTestW)
        nClasses = 1;
        if multiclass:
            nClasses = len(shortBkgNames)+1
        for classIndex in range(nClasses):
            plotResponse(testTrainResponses[modelName], sigBkgTestTrainW, newShortBkgNames, suffix=modelName+'_'+"_".join(newShortBkgNames), classIndex=classIndex)

    # This is a bad hack. Should think about how to do this better. 
    if 'FourVec' not in branchSetKey:
        allVars = [np.concatenate((allData[0], extraVars[0]), axis=1), [np.concatenate((allData[1][bkgI], extraVars[1][bkgI]), axis=1) for bkgI in range(len(allData[1]))]]
    else:
        # This is no good. Somehow the extra variables are somehow isn't being returned correctly for the backgrounds (should be a list of backgrounds).
        allVars = [extraVars[0], [extraVars[1]]]
        branches = [branchName for branchName in sorted(extraBranchSets[extraBranchKey])]

    for classIndex in range(nClasses):
        className = 'sigClass'
        if classIndex > 0:
            className = newShortBkgNames[classIndex-1]+'Class'
            if nClasses == 2:
                className = 'CombBkg'

        for bkgI in range(len(sigBkgEffs.values()[0][-1][classIndex])):
            sigEffs = {}
            bkgEffs = {}
            for modelName in models:
                sigEffs[modelName] = sigBkgEffs[modelName][-2][classIndex]                
                bkgEffs[modelName] = sigBkgEffs[modelName][-1][classIndex][bkgI]
            for region in yieldInfo:
                sigEffs[region] = [yieldInfo[region][6]]
                bkgEffs[region] = [yieldInfo[region][8][bkgI]]
            mvcCuts = plotSignifVsEff(sigPrecutYield, bkgPrecutYield[bkgI], sigEffs,
                                      bkgEffs, db=dB, suffix='_'+className+'_'+newShortBkgNames[bkgI], desiredSigEff=0.5)
            for modelName in models:
                for bkgI in range(len(allResponses[modelName][1])):
                    sigResponse = allResponses[modelName][0][:,classIndex]
                    bkgResponse = allResponses[modelName][1][bkgI][:,classIndex]
                    plotVars((sigResponse, bkgResponse), [allVars[0], allVars[1][bkgI]], branches, mvcCuts[modelName], binning, suffix='_'+className+'_'+newShortBkgNames[bkgI], weights=[allWeights[0], allWeights[1][bkgI]], doLog=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a MVC for stop signal/background discrimination.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--train', action='store_true', help="Train models.")
    parser.add_argument('--sigFName', type=str, help='Location of signal file', default='/data/atlasfs02/a/users/whopkins/stopSamples/mc15_13TeV.387168.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_600_300.merge.DAOD_SUSY1.e3969_a766_a821_r7676_p2666Ext.root')
    parser.add_argument('--bkgFNames', type=str, help='Location of background files', default='/data/atlasfs02/a/users/whopkins/stopSamples/ZSherpa221Ext.root')
    parser.add_argument('--compRegions', type=str, help='Regions to use for comparison, comma delimited.', default='SRB,SRB_noMtBMin,SRB_noMtBMin_tauveto,SRB_MtBMin200_tauveto')

    parser.add_argument('--h5Path', type=str, help='Location of signal and background files that have already been converted.', default='/data/atlasfs02/a/users/whopkins/stopSamples/')

    parser.add_argument('--sigTreeName', type=str, help='Signal tree name', default='stop_0Lep')
    parser.add_argument('--bkgTreeName', type=str, help='Background tree name', default='stop_0Lep')
    
    parser.add_argument('--updateSamples', action='store_true', help="Update h5 files by redoing ROOT conversion.")
    parser.add_argument('--updateResponse', action='store_true', help="Update response of samples to model.")
    parser.add_argument('--multiclass', action='store_true', help="Do multiclassification.")
    parser.add_argument('--branches', type=str, default="stopVars", help='Key to use for branch dictionary')
    parser.add_argument('--extraBranchKey', type=str, default="", help='Key to use for extra branches to be used (not for training but for studies)')
    
    parser.add_argument('--modelNames', type=str, help='Name of models. Comma delimited if used for tested.')
    parser.add_argument('--modelFPath', type=str, default='/data/atlasfs02/a/users/whopkins/stopXGBoostModels/', help='Path where model should be saved.')

    parser.add_argument('--shortSigName', type=str, default='stop_600_300', help='Short name used to describe signal h5 file.')
    parser.add_argument('--shortBkgNames', type=str, default='zJets', help='Short names used to describe background h5 file. Comma delimited for multiple backgrounds.')

    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=10000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=4000)
    parser.add_argument('--precutKey', type=str, help='Preselection cut to use', default='precutSR2B')
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--suffix', type=str, help='suffix to identify different grid files and model files', default="")
    parser.add_argument('--metric', type=str, help='Metric used for training', default="acc")
    parser.add_argument('--dB', type=float, help='Background uncertainty', default=0.15)

    args = parser.parse_args() 
    bkgFNames = [bkgFName for bkgFName in args.bkgFNames.split(',')]
    shortBkgNames = [shortBkgName for shortBkgName in args.shortBkgNames.split(',')]
    compRegions = [compRegion for compRegion in args.compRegions.split(',')]
    if 'train' in args.trainTest:
        train([modelName for modelName in args.modelNames.split(",")], args.modelFPath, args.h5Path, args.sigFName,
              bkgFNames, epochs=args.epochs, batchSize=args.batchSize, nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp,
              branchSetKey=args.branches, extraBranchKey=args.extraBranchKey, precutKey=args.precutKey,
              shortSigName=args.shortSigName, shortBkgNames=shortBkgNames, updateSamples=args.updateSamples,
              multiclass=args.multiclass, metric=args.metric);
    if 'test' in args.trainTest:
        testModel(args.h5Path, args.sigFName, bkgFNames, args.modelFPath, [modelName for modelName in args.modelNames.split(",")],
                  nTrainSamp=args.nTrainSamp, nTestSamp=args.nTestSamp, precutKey=args.precutKey, branchSetKey=args.branches,
                  extraBranchKey=args.extraBranchKey, shortSigName=args.shortSigName, shortBkgNames=shortBkgNames,
                  updateResponse=args.updateResponse, dB=args.dB, multiclass=args.multiclass, compRegions=compRegions);
   


