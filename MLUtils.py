"""@package docstring
This package has various utility functions to convert ROOT files to numpy arrays and to evaluate the 
performance of a model including rate plots, trigger turn-on curves and ROCs. It also allows for analyzing 
different CNN layers and uses a deep draw like function to understand the features a particular layer is
sensitivie to.
"""

from root_numpy import rec2array, tree2array
import numpy as np
import h5py, matplotlib, cutStrings, sys, os, shutil, glob, re, math, ROOT
from ROOT import TEfficiency, TH1F, gROOT, TCanvas, gPad, RooStats, TMVA, TFile, TLine, TLegend, gStyle
from setStyle import setStyle
np.random.seed(1) 
from sklearn.model_selection import train_test_split
from matplotlib.colors import LogNorm
import tensorflow as tf
import matplotlib.pyplot as plt
from keras import backend as K
import cPickle as pickle

from keras.preprocessing import sequence

# This is if we really want to turn of the GPU
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
# os.environ["CUDA_VISIBLE_DEVICES"] = ""

font = {'size'   : 14}
matplotlib.rc('font', **font)

from matplotlib.font_manager import FontProperties

ROOT.gErrorIgnoreLevel = ROOT.kError;
gROOT.SetBatch(True);
gROOT.ForceStyle()
setStyle();
gROOT.ForceStyle()
    
def getSigBkgEffFromCut(cut, sigFName, bkgFNames, sigTreeName='stop_0LepExt', bkgTreeName='stop_0LepExt', db=0.3, precutKey="1", update=False, suffix=''):
    """ Get signal and background yields, efficiencies, and signficances given a cut and precut. """ 

    gROOT.SetBatch(True)
    # First check that we don't already have a pickle
    outInfo = {}
    yieldFName = 'yields'
    if suffix != '':
        yieldFName+='_'+suffix
    yieldFName += '.pkl'
    if os.path.exists(yieldFName) and not update:
        outInfo = pickle.load(open(yieldFName))
        
    if sigFName not in outInfo.keys() or cut not in outInfo[sigFName] or precutKey not in outInfo[sigFName] or update:
        sigF = TFile.Open(sigFName);
        sigTree = sigF.Get(sigTreeName)
        
        sigHist = TH1F('sig', '', 1, 0, 2)
        sigTree.Draw("1>>+"+sigHist.GetName(), cut)
        sigYieldCut = sigHist.Integral()
        sigHist.Reset("ICE");
        sigTree.Draw("1>>+"+sigHist.GetName(), precutKey)
        sigYield = sigHist.Integral()
        if sigFName not in outInfo:
            outInfo[sigFName] = {precutKey:sigYield, cut:sigYieldCut}
        else:
            outInfo[sigFName][precutKey] = sigYield
            outInfo[sigFName][cut] = sigYieldCut
        sigF.Close()
    else:
        sigYieldCut = outInfo[sigFName][cut];
        sigYield = outInfo[sigFName][precutKey];

    bkgFs = {}
    bkgTrees = {}
    
    bkgYieldCut = 0;
    bkgYield = 0;
    bkgYields = {}
    bkgYieldsCut = {}
    bkgHist = None
    
    for bkgFName in bkgFNames:
        if bkgFName not in outInfo.keys() or cut not in outInfo[bkgFName] or precutKey not in outInfo[bkgFName] or update:
            print outInfo.keys()
            bkgFs[bkgFName] = TFile.Open(bkgFName) 
            bkgTrees[bkgFName] = bkgFs[bkgFName].Get(bkgTreeName)
            if bkgHist == None:
                bkgHist = TH1F('bkg', '', 1, 0, 2)
            bkgHist.Reset("ICE")
            bkgTrees[bkgFName].Draw("1>>+"+bkgHist.GetName(), cut)
            
            bkgYieldCut+=bkgHist.Integral()
            bkgYieldsCut[bkgFName] = bkgHist.Integral()
            bkgHist.Reset("ICE")
            bkgTrees[bkgFName].Draw("1>>+"+bkgHist.GetName(), precutKey)
            bkgYield+=bkgHist.Integral()
            bkgYields[bkgFName] = bkgHist.Integral()
            #outInfo[bkgFName] = {cut:bkgYieldsCut[bkgFName], precutKey:bkgYields[bkgFName]}
            if bkgFName not in outInfo:
                outInfo[bkgFName] = {precutKey:bkgYields[bkgFName], cut:bkgYieldsCut[bkgFName]}
            else:
                outInfo[bkgFName][precutKey] = bkgYields[bkgFName]
                outInfo[bkgFName][cut] = bkgYieldsCut[bkgFName]
            bkgFs[bkgFName].Close()
        else:
            bkgYieldsCut[bkgFName] = outInfo[bkgFName][cut];
            bkgYields[bkgFName] = outInfo[bkgFName][precutKey];
            bkgYield+=bkgYields[bkgFName]
            bkgYieldCut+=bkgYieldsCut[bkgFName]

    sigEff = sigYieldCut/sigYield
    bkgEff = bkgYieldCut/bkgYield
    signif = RooStats.NumberCountingUtils.BinomialObsZ(sigYieldCut+bkgYieldCut, bkgYieldCut, db)

    bkgEffs = []
    signifs = []

    for bkgFName in bkgFNames:
        bkgEffs.append(bkgYieldsCut[bkgFName]/bkgYields[bkgFName])
        signifs.append(RooStats.NumberCountingUtils.BinomialObsZ(sigYieldCut+bkgYieldsCut[bkgFName], bkgYieldsCut[bkgFName], db))

    pickle.dump(outInfo, open(yieldFName, 'w'))
    return (sigYield, bkgYield, bkgYields, sigYieldCut, bkgYieldCut, bkgYieldsCut, sigEff, bkgEff, bkgEffs, signif, signifs)

def getArraysFromFiles(fPatterns, branches, treeName, precutKey, weightBranches, lumi, maxNumJets=-1, friendName='', bkg=False, h5FName=None, storeName=''):
    """ This function creates numpy arrays from input ROOT files."""
    
    if bkg:
        bkgTypes = [glob.glob(fPattern) for fPattern in fPatterns]
        bkgs = []
        bkgsWeights = []

        for bkgFNames in bkgTypes:
            bkgEvts = np.empty([0,len(branches)])
            bkgTempWeights = np.empty([0,len(weightBranches)])

            for bkgFName in bkgFNames:
                bkgFile = TFile.Open(bkgFName);
                bkgTree = bkgFile.Get(treeName);
                if friendName != '':
                    bkgTreeExt = bkgFile.Get(friendName)
                    bkgTreeExt.AddFriend(bkgTree);
                else:
                    bkgTreeExt = bkgTree
                bkgEvts = np.concatenate((bkgEvts, rec2array(tree2array(bkgTreeExt, branches, cutStrings.allCuts[precutKey]))))

                if len(weightBranches) > 0:
                    bkgTempWeights = np.concatenate((bkgTempWeights, rec2array(tree2array(bkgTreeExt, weightBranches, cutStrings.allCuts[precutKey]))))
                bkgFile.Close();

            #Flatten if necessary
            if maxNumJets>0:
                newBkgEvts = np.zeros([np.shape(bkgEvts)[0],len(branches)*maxNumJets])
                for eventIndex in range(0,len(bkgEvts)):
                    for myBranchIndex in range(0, len(bkgEvts[eventIndex])):
                        for jetIndex in range(0, min(maxNumJets, len(bkgEvts[eventIndex][myBranchIndex]))):
                            newBkgEvts[eventIndex][maxNumJets*myBranchIndex+jetIndex] = bkgEvts[eventIndex][myBranchIndex][jetIndex]
                bkgEvts = newBkgEvts

            bkgs.append(bkgEvts)

            if len(weightBranches) > 0:
                bkgTempWeights = np.multiply.reduce(bkgTempWeights, axis=1)*1000*lumi    
                bkgsWeights.append(bkgTempWeights)
            else:
                bkgsWeights.append(np.ones(bkgEvts.shape[0]))
                
        data = bkgs
        weights = bkgsWeights
    else:
        sigFNames = glob.glob(fPatterns)
        signal = np.empty([0,len(branches)])
        if len(weightBranches) > 1:
            sigWeights = np.empty([0,len(weightBranches)])
        else:
            sigWeights = np.empty([0,1])

        for sigFName in sigFNames:
            print sigFName
            sigFile = TFile.Open(sigFName);
            sigTree = sigFile.Get(treeName);
            if friendName != '':
                sigTreeExt = sigFile.Get(friendName)        
                sigTreeExt.AddFriend(sigTree);
            else:
                sigTreeExt = sigTree
            sigArray = rec2array(tree2array(sigTreeExt, branches, cutStrings.allCuts[precutKey]))
            signal = np.concatenate((signal, sigArray))
            if len(weightBranches) > 0:
                sigWeights = np.concatenate((sigWeights,rec2array(tree2array(sigTreeExt, weightBranches, cutStrings.allCuts[precutKey]))))

            sigFile.Close();
       
        if maxNumJets>0:
            newSignal = np.zeros([np.shape(signal)[0],len(branches)*maxNumJets])
            for eventIndex in range(0,np.shape(signal)[0]):
                for myBranchIndex in range(0, len(signal[eventIndex])):
                    for jetIndex in range(0, min(maxNumJets, len(signal[eventIndex][myBranchIndex]))):
                        newSignal[eventIndex][maxNumJets*myBranchIndex+jetIndex] = signal[eventIndex][myBranchIndex][jetIndex]
            signal = newSignal

        if len(weightBranches) > 0:
            sigWeights = np.multiply.reduce(sigWeights, axis=1)*1000*lumi
        else:
            sigWeights = np.ones(signal.shape[0])
            
        data = signal
        weights = sigWeights

    if h5FName:
        h5f = h5py.File(h5FName, 'w')
        print data[data.dtype==object]
        h5f.create_dataset(storeName, data=data)
        h5f.create_dataset(storeName+'Weight', data=weights)
        h5f.close();
    return (data, weights)


def getEtaPhiMap(fName, branches, treeName="maps"):
    """ To avoid duplicatation of identical values of the tower coordinates (we have many events with exactly the same coordinates)
    the coordinates are saved in a separate tree. Fetch this tree and put everything in a numpy array."""
    mapFile = TFile.Open(fName);
    mapTree = mapFile.Get(treeName);

    etaPhiMap = rec2array(tree2array(mapTree, branches))
    mapFile.Close()
    return etaPhiMap[0]

def makeGridPhysObj(data, xBins, yBins, etaPhiMap):
    """ This function turns a numpy array into a 2D grid with a multi-dimensional z-axis (intensity). 
    The last two elements of data are the x and y values. """
    grids = []
    nXPix=len(xBins)-1
    nYPix=len(yBins)-1
    
    for evt in data:
        grid = np.zeros((nXPix, nYPix, data.shape[1]-2))
        # Loop over objects (jets/electrons/muons)
        for objI in range(evt[0].size):            
            x = evt[-2][objI]
            y = evt[-1][objI]
            xBin = np.digitize(x, xBins)-1
            yBin = np.digitize(y, yBins)-1
            z = []
            for zVal in evt[:-2]:
                z.append(zVal[objI])
            if grid.shape[0] > xBin and grid.shape[1] > yBin:
                grid[xBin][yBin] = z
        
        grids.append(grid)
    grids = np.array(grids)

    return grids

def makeGridGFEX(data, xBins, yBins, etaPhiMap):
    """ Turn gFEX energy deposits and eta/phi map into a numpy array. Future versions of this functions should
    allow for multidimensional intenisity information. Particularly to allow for the separation of EM and hadronic energy deposits."""
    grids = []
    nXPix=len(xBins)-1
    nYPix=len(yBins)-1
    for evt in data:
        grid = np.zeros((nXPix, nYPix, 1))
        # Loop over objects (gTowers)
        for objI in range(evt[0].size):
            x = etaPhiMap[0][objI]
            y = etaPhiMap[1][objI]
            xBin = np.digitize(x, xBins)-1
            yBin = np.digitize(y, yBins)-1
            if grid.shape[0] > xBin and grid.shape[1] > yBin:
                grid[xBin][yBin] = evt[0][objI]
        
        grids.append(grid)
    grids = np.array(grids)

    return grids

def getSubset(signal, bkgs, sigWeights, bkgsWeights, sigExtraVars, bkgsExtraVars, nEvts):
    # Now randomly choose events
    reduction = 1.0*(nEvts)/signal.shape[0]
    print "signal reduction =", round(reduction,2)
    print "input signal size=", signal.shape
    if reduction > 1.0:
        print "Problem you are trying to get a subset that is larger than the set. Setting nEvts to the number of events in signal set."
        nEvts = signal.shape[0]
    sigRows = np.random.randint(signal.shape[0], size=nEvts)
    signalSubset = signal[sigRows];
    sigExtraVarsSubset = sigExtraVars[sigRows];
    sigSubsetWeights = sigWeights[sigRows]
    
    totalBkgSubset, totalBkgExtraVarsSubset, totalBkgSubsetWeights = sumBkgs(bkgs, bkgsExtraVars, bkgsWeights, nEvts)

    bkgsSubset = []
    bkgsWeightsSubset = []
    bkgsExtraVarsSubset = []
    for bkgI in range(len(bkgs)):
         bkgRows = np.random.randint(bkgs[bkgI].shape[0], size=nEvts)
         bkgsSubset.append(bkgs[bkgI][bkgRows]);
         bkgsExtraVarsSubset.append(bkgsExtraVars[bkgI][bkgRows]);
         bkgsWeightsSubset.append(bkgsWeights[bkgI][bkgRows])
    
    
    minBkgSize = min([bkg.shape[0] for bkg in bkgs])
    print minBkgSize
    print "bkg reduction =", round(1.0*(nEvts)/minBkgSize,2)
    print "Smallest input bkg shape=", minBkgSize
    return (signalSubset, totalBkgSubset, bkgsSubset, sigSubsetWeights, totalBkgSubsetWeights, bkgsWeightsSubset, sigExtraVarsSubset, totalBkgExtraVarsSubset, bkgsExtraVarsSubset)

def sumBkgs(bkgs, bkgsExtraVars, bkgsWeights, nEvts=-1):
    """ Sum the backgrounds into one total background """
    shape = [0]
    shape.extend(bkgs[0].shape[1:])
    totalBkg = np.empty(shape)

    extraVarsShape = [0]
    extraVarsShape.extend(bkgsExtraVars[0].shape[1:])
    totalBkgExtraVars = np.empty(extraVarsShape)
    
    if len(bkgsWeights[0].shape) > 1:   
        totalBkgWeights = np.empty([0, bkgsWeights[0].shape[1]])
    else:
        totalBkgWeights = np.empty([0])

    for bkgI in range(len(bkgs)): 
        totalBkg          = np.concatenate((totalBkg, bkgs[bkgI]))
        totalBkgExtraVars = np.concatenate((totalBkgExtraVars, bkgsExtraVars[bkgI]))
        totalBkgWeights   = np.concatenate((totalBkgWeights, bkgsWeights[bkgI]))
    if nEvts > 0:
        print totalBkg.shape[0], nEvts
        bkgRows           = np.random.randint(totalBkg.shape[0], size=nEvts)
        totalBkg          = totalBkg[bkgRows]
        totalBkgExtraVars = totalBkgExtraVars[bkgRows]
        totalBkgWeights   = totalBkgWeights[bkgRows]

    return (totalBkg, totalBkgExtraVars, totalBkgWeights)
    
def prepROOTDataML1D(nTrainSamp, nTestSamp, branches, branchSetKey, trainBranchIndex, h5Path, sigFName, bkgFNames, sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', precutKey="NoCut", weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], lumi=36, update=False, shortSigName='sig', shortBkgNames=['bkg'], randomSeed=1):
    """!
    Convert signal root file to numpy if necessary
    """
    
    h5FName = h5Path+'branches_'+branchSetKey+'_'+precutKey+'_'+shortSigName+'_'+'_'.join(shortBkgNames)+'.h5'
    if os.path.exists(h5FName) and not update:
        print "Loading previous data set from", h5FName
        h5F = h5py.File(h5FName, 'r')
        signal = h5F['signal'][:]
        sigWeights = h5F['sigWeights'][:]
        sigExtraVars  = h5F['sigExtraVars'][:]
        bkgs = []
        bkgsWeights = []
        bkgsExtraVars = []
        for shortBkgName in shortBkgNames:
            bkgs.append(h5F[shortBkgName][:])    
            bkgsWeights.append(h5F[shortBkgName+'Weights'][:])
            bkgsExtraVars.append(h5F[shortBkgName+'ExtraVars'][:])
        h5F.close()
    else:
        print 'Converting ROOT trees to numpy arrays'
        (signal, sigWeights) = getArraysFromFiles(sigFName, branches, sigTreeName, precutKey, weightBranches, lumi, friendName=sigFriendName, bkg=False);
        (bkgs, bkgsWeights) = getArraysFromFiles(bkgFNames, branches, bkgTreeName, precutKey, weightBranches, lumi, friendName=bkgFriendName, bkg=True);
        sigExtraVars = np.array(signal[:,trainBranchIndex:], dtype=float)
        signal = signal[:,0:trainBranchIndex]

        bkgsExtraVars = []
        bkgsForTraining = []
        for bkg in bkgs:
            bkgsExtraVars.append(np.array(bkg[:,trainBranchIndex:], dtype=float))
            bkgsForTraining.append(bkg[:,0:trainBranchIndex])

        bkgs = bkgsForTraining
        
        h5f = h5py.File(h5FName, 'w')
        h5f.create_dataset('signal', data=signal)
        h5f.create_dataset('sigWeights', data=sigWeights)
        h5f.create_dataset('sigExtraVars', data=sigExtraVars)
        for bkgI in range(len(shortBkgNames)):
            h5f.create_dataset(shortBkgNames[bkgI], data=bkgs[bkgI])
            h5f.create_dataset(shortBkgNames[bkgI]+'Weights', data=bkgsWeights[bkgI])
            h5f.create_dataset(shortBkgNames[bkgI]+'ExtraVars', data=bkgsExtraVars[bkgI])
        h5f.close();

    signalSubset, totalBkgSubset, bkgsSubset, sigSubsetWeights, totalBkgSubsetWeights, bkgsWeightsSubset, sigExtraVarsSubset, totalBkgExtraVarsSubset, bkgsExtraVarsSubset = getSubset(signal, bkgs, sigWeights, bkgsWeights, sigExtraVars, bkgsExtraVars, nTrainSamp+nTestSamp)
    totalBkg, bkgExtraVars, totalBkgWeights = sumBkgs(bkgs, bkgsExtraVars, bkgsWeights)
    xData = np.concatenate((signalSubset, totalBkgSubset));
    weights = np.concatenate((sigSubsetWeights, totalBkgSubsetWeights))
    yData = np.concatenate((np.zeros(signalSubset.shape[0]), np.ones(totalBkgSubset.shape[0])))
    extraVarsSubSet = np.concatenate((sigExtraVarsSubset, totalBkgExtraVarsSubset))
    xDataMultiClass = signalSubset
    weightsMultiClass = sigSubsetWeights
    yDataMultiClass = np.zeros(signalSubset.shape[0])
    nClasses = 1;
    for bkgI in range(len(bkgsSubset)):
        xDataMultiClass = np.concatenate((xDataMultiClass, bkgsSubset[bkgI]))
        weightsMultiClass = np.concatenate((weightsMultiClass, bkgsWeightsSubset[bkgI]))
        yDataMultiClass = np.concatenate((yDataMultiClass, np.ones(bkgsSubset[bkgI].shape[0])*(bkgI+1)))
        nClasses+=1

    (xTrain, xTest, yTrain, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest) = train_test_split(xData, yData, weights,  extraVarsSubSet, stratify=yData, test_size=nTestSamp*2, train_size=nTrainSamp*2, random_state=randomSeed+36)
    (xTrainMultiClass, xTestMultiClass, yTrainMultiClass, yTestMultiClass, weightsTrainMultiClass, weightsTestMultiClass) = train_test_split(xDataMultiClass, yDataMultiClass, weightsMultiClass, stratify=yDataMultiClass, test_size=nTestSamp*nClasses, train_size=nTrainSamp*nClasses, random_state=randomSeed)
    
    return (xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest, signal, bkgs,  sigExtraVars, bkgsExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset, sigWeights, totalBkgWeights, (xTrainMultiClass, yTrainMultiClass, xTestMultiClass, yTestMultiClass, weightsTrainMultiClass, weightsTestMultiClass, bkgs, bkgsWeights))


def prepROOTDataMLFourVecs(nTrainSamp, nTestSamp, branches, branchSetKey, trainBranchIndex, h5Path, sigFName, bkgFNames, sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', precutKey="NoCut", weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], lumi=36, update=False, shortSigName='sig', shortBkgNames=['bkg'], randomSeed=1, truncArrayLength=10):
    """! 
    Convert data from a ROOT file given a set of branches into a multi-dimensional array. Branches that are vectors
    require special treatment. This function could potentially be merged back with the function above. Only one dimensional vectors are
    currently supported. The ordering of the output is based on LSTM required ordering. 
    """
    
    h5FName = h5Path+'branches_'+branchSetKey+'_'+precutKey+'_'+shortSigName+'_'+'_'.join(shortBkgNames)+'.h5'
    if os.path.exists(h5FName) and not update:
        print "Loading previous data set from", h5FName
        h5F = h5py.File(h5FName, 'r')
        signal = h5F['signal'][:]
        sigWeights = h5F['sigWeights'][:]
        sigExtraVars  = h5F['sigExtraVars'][:]
        bkgs = []
        bkgsWeights = []
        bkgsExtraVars = []
        for shortBkgName in shortBkgNames:
            bkgs.append(h5F[shortBkgName][:])    
            bkgsWeights.append(h5F[shortBkgName+'Weights'][:])
            bkgsExtraVars.append(h5F[shortBkgName+'ExtraVars'][:])
        h5F.close()
    else:
        (signal, sigWeights) = getArraysFromFiles(sigFName, branches, sigTreeName, precutKey, weightBranches, lumi, friendName=sigFriendName, bkg=False);
        (bkgs, bkgsWeights) = getArraysFromFiles(bkgFNames, branches, bkgTreeName, precutKey, weightBranches, lumi, friendName=bkgFriendName, bkg=True);
        sigExtraVars = np.array(signal[:,trainBranchIndex:], dtype=float)
        signal = np.array(signal[:,0:trainBranchIndex])
        bkgsExtraVars = []
        bkgsForTraining = []
        newBkgs = []
        for bkg in bkgs:
            bkgsExtraVars.append(np.array(bkg[:,trainBranchIndex:], dtype=float))
            trainBkgs = bkg[:,0:trainBranchIndex]
            
            # There has to be a better way to do this. rec2array returns an numpy array of numpy arrays when handed a branch
            # that is a std::vector. Looping over the elements is a very inefficient and an unnumpy way of doing this...
            # The order is chosen here to work with an LSTM which requires the indices to be: sample, time step, feature.
            # In our case the sample = event, time step = jet/object indes, feature = pt/eta/phi.
            
            newBkg = np.empty((trainBkgs.shape[0], truncArrayLength, trainBkgs.shape[1]))
            newBkg[:,:,:] = np.nan
            for evtI in range(trainBkgs.shape[0]):
                for varI in range(trainBkgs.shape[1]):
                    evtInfo = sequence.pad_sequences(trainBkgs[evtI], maxlen=truncArrayLength, padding='post', dtype='float32', truncating='post')
                    for objI in range(truncArrayLength):
                        newBkg[evtI,objI,varI] = evtInfo[varI,objI]
            newBkgs.append(newBkg)
        bkgs = newBkgs

        # Same for signal
        newSignal = np.empty((signal.shape[0], truncArrayLength, signal.shape[1]))
        newSignal[:,:,:] = np.nan
        for evtI in range(signal.shape[0]):
            for varI in range(signal.shape[1]):
                evtInfo = sequence.pad_sequences(signal[evtI], maxlen=truncArrayLength, padding='post', dtype='float32', truncating='post')
                for objI in range(truncArrayLength):
                    newSignal[evtI,objI,varI] = evtInfo[varI,objI]
                
        signal = newSignal

        h5f = h5py.File(h5FName, 'w')
        h5f.create_dataset('signal', data=signal)
        h5f.create_dataset('sigWeights', data=sigWeights)
        h5f.create_dataset('sigExtraVars', data=sigExtraVars)

        for bkgI in range(len(shortBkgNames)):
            h5f.create_dataset(shortBkgNames[bkgI], data=bkgs[bkgI])
            h5f.create_dataset(shortBkgNames[bkgI]+'Weights', data=bkgsWeights[bkgI])
            h5f.create_dataset(shortBkgNames[bkgI]+'ExtraVars', data=bkgsExtraVars[bkgI])
        h5f.close();

    signalSubset, totalBkgSubset, bkgsSubset, sigSubsetWeights, totalBkgSubsetWeights, bkgsWeightsSubset, sigExtraVarsSubset, totalBkgExtraVarsSubset, bkgsExtraVarsSubset = getSubset(signal, bkgs, sigWeights, bkgsWeights, sigExtraVars, bkgsExtraVars, nTrainSamp+nTestSamp)
    totalBkg, bkgExtraVars, totalBkgWeights = sumBkgs(bkgs, bkgsExtraVars, bkgsWeights)
    
    xData = np.concatenate((signalSubset, totalBkgSubset));
    weights = np.concatenate((sigSubsetWeights, totalBkgSubsetWeights))
    yData = np.concatenate((np.zeros(signalSubset.shape[0]), np.ones(totalBkgSubset.shape[0])))
    extraVarsSubSet = np.concatenate((sigExtraVarsSubset, totalBkgExtraVarsSubset))

    xDataMultiClass = signalSubset
    weightsMultiClass = sigSubsetWeights
    yDataMultiClass = np.zeros(signalSubset.shape[0])
    nClasses = 1;
    for bkgI in range(len(bkgsSubset)):
        xDataMultiClass = np.concatenate((xDataMultiClass, bkgsSubset[bkgI]))
        weightsMultiClass = np.concatenate((weightsMultiClass, bkgsWeightsSubset[bkgI]))
        yDataMultiClass = np.concatenate((yDataMultiClass, np.ones(bkgsSubset[bkgI].shape[0])*(bkgI+1)))
        nClasses+=1

    (xTrain, xTest, yTrain, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest) = train_test_split(xData, yData, weights,  extraVarsSubSet, stratify=yData, test_size=nTestSamp*2, train_size=nTrainSamp*2, random_state=randomSeed)

    (xTrainMultiClass, xTestMultiClass, yTrainMultiClass, yTestMultiClass, weightsTrainMultiClass, weightsTestMultiClass) = train_test_split(xDataMultiClass, yDataMultiClass, weightsMultiClass, stratify=yDataMultiClass, test_size=nTestSamp*nClasses, train_size=nTrainSamp*nClasses, random_state=randomSeed)
    
    return (xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest, signal, totalBkg,  sigExtraVars, bkgExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset, sigWeights, totalBkgWeights, (xTrainMultiClass, yTrainMultiClass, xTestMultiClass, yTestMultiClass, weightsTrainMultiClass, weightsTestMultiClass, bkgs, bkgsWeights))

def plotGrid(inGrid, chosenEvent, outName, units='GeV', transpose=True, vmin=None, vmax=None, sf=1/1000.):   
    exampleGrid = inGrid[chosenEvent, 0, :, :]*sf
    if transpose:
       exampleGrid = exampleGrid.T
    exGTowerEt = np.ndarray.max(exampleGrid, axis=(0,1))

    plt.clf()
    try:
        fig, ax = plt.subplots()
        im = plt.imshow(exampleGrid, origin='lower', vmin=vmin, vmax=vmax, norm=LogNorm())
        cb = fig.colorbar(im)
        cb.set_label(units)
        ax.set_xlabel('$\eta$-bin')
        ax.set_ylabel('$\phi$-bin')
        ax.xaxis.set_label_coords(0.9, -0.08)
        ax.yaxis.set_label_coords(-0.15, 0.9)
        plt.savefig(outName+'_log.pdf', bbox_inches='tight')
    except:
        print "Can't make log plots."
    plt.clf()
    fig, ax = plt.subplots()
    im = plt.imshow(exampleGrid, origin='lower', vmin=vmin, vmax=vmax)
    cb = fig.colorbar(im)
    cb.set_label(units)
    ax.set_xlabel('$\eta$-bin')
    ax.set_ylabel('$\phi$-bin')
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.15, 0.9)
    plt.savefig(outName+'.pdf', bbox_inches='tight')

    plt.close('all')
    
def prepROOTDataML2D(nTrainSamp, nTestSamp, branches, sigFName, bkgFNames, etaBins, phiBins, sigTreeName='stop_0Lep', bkgTreeName='stop_0Lep', sigFriendName='stop_0LepExt', bkgFriendName='stop_0LepExt', precutKey="NoCut", weightBranches=["ThreeBodyWeightNum","JVTSF","sherpaNJetsWeight","BTagSF","PileupWeight","FinalXSecWeight","GenWeight"], lumi=36, update=False, suffix='', extraBranches=[], shortSigName='sig', shortBkgName='bkg', randomSeed=1):
    """ Convert the ROOT data to numpy arrays and preprocess it to form a grid in phi and eta """
    sigH5FName = 'grid2D'+'_'+shortSigName+'_'+suffix+'_'+precutKey+'.h5'
    bkgH5FName = 'grid2D'+'_'+shortBkgName+'_'+suffix+'_'+precutKey+'.h5'

    nEtaPix = len(etaBins)-1
    nPhiPix = len(phiBins)-1
    
    # Make grid where the first indexes are pT, phiBin, etaBin
    if "gfex" in suffix.lower():
        makeGrid = makeGridGFEX
    else:
        etaPhiMap = None;
        makeGrid = makeGridPhysObj
         
    if os.path.exists(sigH5FName) and not update:
        print "Loading previous data set from", sigH5FName
        h5F           = h5py.File(sigH5FName, 'r')
        sigGrid       = h5F['sigGrid'][:]
        sigWeights    = h5F['sigWeight'][:]
        sigExtraVars  = h5F['sigExtraVars'][:]
        h5F.close()
    else:
        (signal, sigWeights) = getArraysFromFiles(sigFName, branches+extraBranches, sigTreeName, precutKey, weightBranches, lumi, friendName=sigFriendName, bkg=False);

        sigExtraVars = np.array(signal[:,len(branches):], dtype=float)
        signal = signal[:,0:len(branches)]

        if "gfex" in suffix.lower():
            etaPhiMap = getEtaPhiMap(glob.glob(sigFName)[0], ["gTowerEtaGeometric", "gTowerPhiGeometric"])
        
        print "Making signal grid."
        sigGrid = makeGrid(signal, etaBins, phiBins, etaPhiMap)
        h5f = h5py.File(sigH5FName, 'w')
        h5f.create_dataset('sigGrid', data=sigGrid)
        h5f.create_dataset('sigWeight', data=sigWeights)
        h5f.create_dataset('sigExtraVars', data=sigExtraVars)
        h5f.close()
        
    if os.path.exists(bkgH5FName) and not update:
        print "Loading previous data set from", bkgH5FName
        h5F           = h5py.File(bkgH5FName, 'r')
        bkgsGrid      = h5F['bkgsGrid'][:]
        bkgsWeights   = h5F['bkgsWeights'][:]
        bkgsExtraVars = h5F['bkgsExtraVars'][:]
        h5F.close()

    else:
        (bkgs, bkgsWeights) = getArraysFromFiles(bkgFNames, branches+extraBranches, bkgTreeName, precutKey, weightBranches, lumi, friendName=bkgFriendName, bkg=True);

        bkgsExtraVars = []
        bkgsForTraining = []
        for bkg in bkgs:
            bkgsExtraVars.append(np.array(bkg[:,len(branches):], dtype=float))
            bkgsForTraining.append(bkg[:,0:len(branches)])

        if "gfex" in suffix.lower():
            etaPhiMap = getEtaPhiMap(glob.glob(bkgFNames[0])[0], ["gTowerEtaGeometric", "gTowerPhiGeometric"])

        print "Making background grids."
        bkgsGrid = []
        for bkg in bkgsForTraining:
            bkgsGrid.append(makeGrid(bkg, etaBins, phiBins, etaPhiMap))
        print "Done making grids."
        h5f = h5py.File(bkgH5FName, 'w')
        h5f.create_dataset('bkgsGrid', data=bkgsGrid)
        h5f.create_dataset('bkgsWeights', data=bkgsWeights)
        h5f.create_dataset('bkgsExtraVars', data=bkgsExtraVars)
                
        h5f.close();

    # Get subset of events that deals with multiple DSIDs in one ROOT file
    sigGridSubset, bkgGridSubset, bkgsGridSubset, sigSubsetWeights, totalBkgSubsetWeights, bkgsWeightsSubset, sigExtraVarsSubset, totalBkgExtraVarsSubset, bkgsExtraVarsSubset = getSubset(sigGrid, bkgsGrid, sigWeights, bkgsWeights, sigExtraVars, bkgsExtraVars, nTrainSamp+nTestSamp)    

    # Sum backgrounds for all events
    bkgGrid, bkgExtraVars, bkgWeights = sumBkgs(bkgsGrid, bkgsExtraVars, bkgsWeights)

    sigGrid = sigGrid.reshape(sigGrid.shape[0], len(branches)-2, nEtaPix, nPhiPix)
    bkgGrid = bkgGrid.reshape(bkgGrid.shape[0], len(branches)-2, nEtaPix, nPhiPix)

    xData = np.concatenate((sigGridSubset, bkgGridSubset));
    yData = np.concatenate((np.ones(sigGridSubset.shape[0]), np.zeros(bkgGridSubset.shape[0])))
    xData = xData.reshape(xData.shape[0], len(branches)-2, nEtaPix, nPhiPix)
    weights = np.concatenate((sigSubsetWeights, totalBkgSubsetWeights))
    extraVarsSubSet = np.concatenate((sigExtraVarsSubset, totalBkgExtraVarsSubset))
  
    (xTrain, xTest, yTrain, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest) = train_test_split(xData, yData, weights, extraVarsSubSet, test_size=nTestSamp*2, train_size=nTrainSamp*2, random_state=randomSeed)

    return (xTrain, yTrain, xTest, yTest, weightsTrain, weightsTest, extraVarsTrain, extraVarsTest, sigGrid, bkgGrid, sigExtraVars, bkgExtraVars, sigExtraVarsSubset, totalBkgExtraVarsSubset, sigWeights, bkgWeights)


        
def getSigBkgEffFromModel(model, testTrainData, testTrainWeights, allData, allWeights, responseFName, binning=[i*.001 for i in range(1001)], update=False, xgb=False, multiclass=True):

    if os.path.exists(responseFName) and not update:
        print "Loading previous responses from", responseFName
        ((sigResponseTrain, bkgResponseTrain, sigResponseTest, bkgResponseTest, allSigResponse, allBkgResponse), (sigEffsTrain, bkgEffsTrain, sigEffsTest, bkgEffsTest, sigEffsAll, bkgEffsAll)) = pickle.load(open(responseFName))
        
    else:
        xTrain, yTrain, xTest, yTest = testTrainData
        xTrainW, xTestW = testTrainWeights
        allSig, allBkg = allData
        allSigW, allBkgW = allWeights
        # Assume that signal has class value of 0
        bkgTest = []
        bkgTrain = []
        bkgTestW = []
        bkgTrainW = []
        sigTest = xTest[yTest==0]
        sigTrain = xTrain[yTrain==0]
        sigTestW = xTestW[yTest==0]
        sigTrainW = xTrainW[yTrain==0]
    
        for classI in range(1, np.unique(yTest).shape[0]):
            bkgTest.append(xTest[yTest==classI])
            bkgTrain.append(xTrain[yTrain==classI])
            bkgTestW.append(xTestW[yTest==classI])
            bkgTrainW.append(xTrainW[yTrain==classI])
            
        if xgb:
            print "Getting response for signal test sample."
            print sigTest.shape
            sigResponseTest = model.predict_proba(sigTest)
            print "Getting response for signal train sample."
            sigResponseTrain = model.predict_proba(sigTrain)
            print "Getting response for all signal sample."            
            allSigResponse = model.predict_proba(allSig)

            bkgResponseTest = []
            bkgResponseTrain = []
            allBkgResponse = []
            for bkgI in range(len(allBkg)):
                print "Getting response for bkg "+str(bkgI)+" test sample."
                allBkgResponse.append(model.predict_proba(allBkg[bkgI]))
                print "Getting response for bkg "+str(bkgI)+" train sample."
                bkgResponseTest.append(model.predict_proba(bkgTest[bkgI]))
                print "Getting response for all bkg "+str(bkgI)+" sample."
                bkgResponseTrain.append(model.predict_proba(bkgTrain[bkgI]))

        else:
            sigResponseTest = model.predict(sigTest)
            sigResponseTrain = model.predict(sigTrain)
            allSigResponse = model.predict(allSig)
            
            bkgResponseTest = []
            bkgResponseTrain = []
            allBkgResponse = []
            for bkgI in range(len(allBkg)):
                print "Getting response for bkg "+str(bkgI)+" test sample."
                allBkgResponse.append(model.predict(allBkg[bkgI]))
                print "Getting response for bkg "+str(bkgI)+" train sample."
                bkgResponseTest.append(model.predict(bkgTest[bkgI]))
                print 'bkg', model.predict(bkgTest[bkgI]), bkgTrain[bkgI].shape
                print "Getting response for all bkg "+str(bkgI)+" sample."
                bkgResponseTrain.append(model.predict(bkgTrain[bkgI]))
        
        sigEffsTest = []
        bkgEffsTest = []

        sigEffsTrain = []
        bkgEffsTrain = []

        sigEffsAll = []
        bkgEffsAll = []

        for classI in range(sigResponseTrain.shape[1]):
            sigEffsTestClass = []
            bkgEffsTestClass = []

            sigEffsTrainClass = []
            bkgEffsTrainClass = []

            sigEffsAllClass = []
            bkgEffsAllClass = []
            for binI in binning:
                sigEffsTestClass.append(np.sum(sigTestW[sigResponseTest[:,classI]>binI])/np.sum(sigTestW))
                sigEffsTrainClass.append(np.sum(sigTrainW[sigResponseTrain[:,classI]>binI])/np.sum(sigTrainW))
                sigEffsAllClass.append(np.sum(allSigW[allSigResponse[:,classI]>binI])/np.sum(allSigW))
            for bkgI in range(len(allBkgResponse)):
                bkgEffTest  = []
                bkgEffTrain = []
                bkgEffAll   = []
                for binI in binning:
                    bkgEffTest.append(np.sum(bkgTestW[bkgI][bkgResponseTest[bkgI][:,classI]>binI])/np.sum(bkgTestW[bkgI]))
                    bkgEffTrain.append(np.sum(bkgTrainW[bkgI][bkgResponseTrain[bkgI][:,classI]>binI])/np.sum(bkgTrainW[bkgI]))
                    bkgEffAll.append(np.sum(allBkgW[bkgI][allBkgResponse[bkgI][:][:,classI]>binI])/np.sum(allBkgW[bkgI]))
                
                bkgEffsTestClass.append(bkgEffTest)
                bkgEffsTrainClass.append(bkgEffTrain)
                bkgEffsAllClass.append(bkgEffAll)

            sigEffsTest.append(sigEffsTestClass)
            sigEffsTrain.append(sigEffsTrainClass)
            sigEffsAll.append(sigEffsAllClass)
     
            bkgEffsTest.append(bkgEffsTestClass)      
            bkgEffsTrain.append(bkgEffsTrainClass)
            bkgEffsAll.append(bkgEffsAllClass)

            pickle.dump(((sigResponseTrain, bkgResponseTrain, sigResponseTest, bkgResponseTest, allSigResponse, allBkgResponse), (sigEffsTrain, bkgEffsTrain, sigEffsTest, bkgEffsTest, sigEffsAll, bkgEffsAll)), open(responseFName, 'w'))
            
    return ((sigResponseTrain, bkgResponseTrain, sigResponseTest, bkgResponseTest, allSigResponse, allBkgResponse), (sigEffsTrain, bkgEffsTrain, sigEffsTest, bkgEffsTest, sigEffsAll, bkgEffsAll))



def getSigBkgEffFromTMVA(sigResponseTrain, bkgResponseTrain, sigResponseTest, bkgResponseTest, binning=[-1+i*.01 for i in range(201)]):

    sigEffsTest = []
    bkgEffsTest = []
    
    sigEffsTrain = []
    bkgEffsTrain = []
    for binI in binning:
        passedCutSig = [sigRes for sigRes in sigResponseTest if sigRes > binI]
        passedCutBkg = [bkgRes for bkgRes in bkgResponseTest if bkgRes > binI]

        sigEff = np.nan;
        bkgEff = np.nan
        if len(sigResponseTest)>0:
            sigEff = 1.0*len(passedCutSig)/len(sigResponseTest)
        if len(bkgResponseTest)>0:
            bkgEff = 1.0*len(passedCutBkg)/len(bkgResponseTest)
        
        sigEffsTest.append(sigEff)
        bkgEffsTest.append(bkgEff)

        passedCutSig = [sigRes for sigRes in sigResponseTrain if sigRes > binI]
        passedCutBkg = [bkgRes for bkgRes in bkgResponseTrain if bkgRes > binI]
        sigEff = np.nan
        bkgEff = np.nan
        if len(sigResponseTrain)>0:
            sigEff = 1.0*len(passedCutSig)/len(sigResponseTrain)
        if len(bkgResponseTrain)>0:
            bkgEff = 1.0*len(passedCutBkg)/len(bkgResponseTrain)
        
        sigEffsTrain.append(sigEff)
        bkgEffsTrain.append(bkgEff)
        
    return (sigEffsTrain, bkgEffsTrain, sigEffsTest, bkgEffsTest)



def plotResponse(responses, weights, bkgNames, binning=[i*.05 for i in range(21)], suffix="", classIndex=0, colors=['blue','green','red', 'black', 'brown', 'purple']):
    """ Plot response numpy arrays """
    # We are only going to consider the score for signal not for background. This should be the first element.
    [sigResponseTrain, tempBkgResponseTrain, sigResponseTest, tempBkgResponseTest] = responses
    [sigTrainW, bkgTrainW, sigTestW, bkgTestW] = weights
    
    sigResponseTest = sigResponseTest[:,classIndex]
    sigResponseTrain = sigResponseTrain[:,classIndex]
    
    bkgResponseTrain = []
    bkgResponseTest = []
    for bkgI in range(len(tempBkgResponseTest)):
        bkgResponseTest.append(tempBkgResponseTest[bkgI][:,classIndex])
        bkgResponseTrain.append(tempBkgResponseTrain[bkgI][:,classIndex])    

    #if len(sigResponseTest) > 0 and len(bkgResponseTest) > 0 and len(sigResponseTrain) > 0 and len(bkgResponseTrain) > 0:
    (sigTestY, bins, p) = plt.hist(sigResponseTest,bins=binning, weights=sigTestW, histtype='step')
    (sigTrainY, bins, p) = plt.hist(sigResponseTrain,bins=binning, weights=sigTrainW, histtype='step')
    sigTestInt = np.sum(sigTestY);
    sigTrainInt = np.sum(sigTrainY);
   
    sigTestNormWeights = sigTestW/sigTestInt
    sigTrainNormWeights = sigTrainW/sigTrainInt

    wBins = np.digitize(sigResponseTest, binning)
    sigErr = np.array([np.sqrt(np.sum(sigTestW[np.where(wBins==binI+1)[0]]**2))/sigTestInt for binI in range(len(binning)-1)]);
   
    (scaledSigTestY, bins, p) = plt.hist(sigResponseTest, bins=binning, histtype='step', label='signal (test)', weights=sigTestNormWeights)

    binCenters = 0.5*(bins[1:]+bins[:-1])

    # We are going to plot the signal classification score for each background.
    bkgTestNormWeights = []
    bkgTrainNormWeights = []
    bkgErrsTest = []
    scaledBkgTestY = []
    for bkgI in range(len(bkgResponseTrain)):
        plt.clf()
        fig, ax = plt.subplots()
        (bkgTestY, bins, p) = plt.hist(bkgResponseTest[bkgI],bins=binning, weights=bkgTestW[bkgI], histtype='step')
        (bkgTrainY, bins, p) = plt.hist(bkgResponseTrain[bkgI],bins=binning, weights=bkgTrainW[bkgI], histtype='step')
        bkgTestInt = np.sum(bkgTestY)
        bkgTrainInt = np.sum(bkgTrainY)

        bkgTestNormWeights.append(bkgTestW[bkgI]/bkgTestInt)
        bkgTrainNormWeights.append(bkgTrainW[bkgI]/bkgTrainInt)

        wBins = np.digitize(bkgResponseTest[bkgI], binning)   
        bkgErrsTest.append(np.array([np.sqrt(np.sum(bkgTestW[bkgI][np.where(wBins==binI+1)[0]]**2))/bkgTestInt for binI in range(len(binning)-1)]))
        (tempScaledBkgTestY, bins, p) = plt.hist(bkgResponseTest[bkgI], bins=binning, histtype='step', weights=bkgTestNormWeights[bkgI])
        scaledBkgTestY.append(tempScaledBkgTestY)
    plt.clf()
    fig, ax = plt.subplots()
    plt.hist(sigResponseTrain,bins=binning, histtype='step', label='signal (train)', weights=sigTrainNormWeights, color=colors[0])
    plt.errorbar(binCenters, scaledSigTestY, fmt='o', yerr=sigErr, label='signal (test)', color=colors[0])

    for bkgI in range(len(bkgResponseTrain)):
        plt.hist(bkgResponseTrain[bkgI],bins=binning, histtype='step', label=bkgNames[bkgI]+' (train)', weights=bkgTrainNormWeights[bkgI], color=colors[bkgI+1])
        plt.errorbar(binCenters, scaledBkgTestY[bkgI], fmt='o', yerr=bkgErrsTest[bkgI],color=colors[bkgI+1])

    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    ax.set_xlabel('MVC output')
    ax.set_ylabel('1/N dN')
    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.4))
    #plt.yscale('log', nonposy='clip')
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.15, 0.9)

    className='sig'
    if classIndex > 0:
        className = bkgNames[classIndex-1]
    plt.savefig('trainTestOutput_'+suffix+'_'+className+'.pdf', bbox_inches='tight')
    
def plotROC(sigEffs, bkgEffs, suffix=""):
    """ Plot signal vs 1-bkg efficiency. """
    fig, ax = plt.subplots()
    for label in sorted(sigEffs.keys()):
        if len(sigEffs[label]) > 1:
            plt.plot(sigEffs[label], [1-bkgEff for bkgEff in bkgEffs[label]], label=label)
        else:
            plt.plot(sigEffs[label], [1-bkgEff for bkgEff in bkgEffs[label]], 's', label=label)
    ax.set_xlabel('Signal efficiency')
    ax.set_ylabel('Background rejections')
    ax.xaxis.set_label_coords(0.85, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)
    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.1))
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)

    outFName = 'ROC'
    if suffix != '':
        outFName+="_"+suffix
        
    plt.savefig(outFName+'.pdf', bbox_inches='tight')
    plt.close('all')


def plotVars(responses, extraVars, branchNames, cut, varBinning, classIndex=0, suffix="", nnOutBinning=[i*.01 for i in range(101)], discrimName='NN', make2DPlots=False, weights=[], doLog=True):
    """ Plot variable distribution for a particular response. Response is not limited to NN output, it could be anything."""
    sigResponse = responses[0]
    bkgResponse = responses[1]

    sigExtraVars = extraVars[0]
    bkgExtraVars = extraVars[1]

    if weights != []:
        sigWeights = weights[0]
        bkgWeights = weights[1]
    else:
        sigWeights = np.ones(sigResponse.shape)
        bkgWeights = np.ones(bkgResponse.shape)
        
    for varI in range(len(branchNames)):
        if not isinstance(branchNames[varI], basestring):
            varName = branchNames[varI][0]
        else:
            varName = branchNames[varI]
        binning = varBinning[varName][0]
        scaling = varBinning[varName][1]
        units = varBinning[varName][2]

        unitStr = "["+units+"]"
        if units == "":
            unitStr = ""

        if make2DPlots:
            plt.clf()
            tempH = plt.hist2d(sigExtraVars[:,varI]*scaling, np.ndarray.flatten(sigResponse), bins=[binning, nnOutBinning], norm=LogNorm(), weights=sigWeights)
            #fig.colorbar(tempH)
            plt.xlabel(varName+" "+unitStr,horizontalalignment='right', x=1.0)
            plt.ylabel(discrimName)
            plt.savefig("sig_"+varName.replace('[','').replace(']', '')+suffix+'_vs_'+discrimName+'.pdf', bbox_inches='tight')
            
            plt.clf()
            tempH = plt.hist2d(bkgExtraVars[:,varI]*scaling, np.ndarray.flatten(bkgResponse), bins=[binning, nnOutBinning], norm=LogNorm(), weights=bkgWeights)
            #fig.colorbar(tempH)
            plt.xlabel(varName+" "+unitStr,horizontalalignment='right', x=1.0)
            plt.ylabel(discrimName)
            plt.savefig("bkg_"+varName.replace('[','').replace(']', '')+suffix+'_vs_'+discrimName+'.pdf', bbox_inches='tight')

        precutSig = sigExtraVars[:,varI]*scaling
        precutBkg = bkgExtraVars[:,varI]*scaling
        postCutSig = sigExtraVars[:,varI][np.ndarray.flatten(sigResponse>cut)]*scaling
        postCutBkg = bkgExtraVars[:,varI][np.ndarray.flatten(bkgResponse>cut)]*scaling

        postCutSigWeights = sigWeights[np.ndarray.flatten(sigResponse>cut)]
        postCutBkgWeights = bkgWeights[np.ndarray.flatten(bkgResponse>cut)]
        
        roundTo = -int(math.floor(math.log10(abs(cut))));
        roundedCut = round(cut, roundTo)
        if roundTo < 0:
            cutStr = "("+discrimName+">%d)" % roundedCut
        else:
            cutStr = "("+discrimName+">"+"{0:.{1}f}".format(round(cut,3), 3)+")"

        plt.clf()
        fig, ax = plt.subplots()

        plt.hist(precutSig,bins=binning, histtype='step', label='signal (no cut)', linestyle=('dashed'), density=True, color='k', weights=sigWeights)
        plt.hist(postCutSig,bins=binning, histtype='step', label='signal '+cutStr, density=True, color='k', weights=postCutSigWeights)
        plt.hist(precutBkg,bins=binning, histtype='step', label='bkg (no cut)', linestyle=('dashed'), density=True, color='r', weights=bkgWeights)
        plt.hist(postCutBkg,bins=binning, histtype='step', label='bkg '+cutStr, density=True, color='r', weights=postCutBkgWeights)

        binSize = (binning[1]-binning[0])
        roundTo = -int(math.floor(math.log10(abs(binSize))));
        roundedBinSize = round(binSize, roundTo)
        if roundTo < 0:
            binSizeStr = "%d" % roundedBinSize
        else:
            binSizeStr = "{0:.{1}f}".format(roundedBinSize, roundTo)
        varNameInYLabel = 'dx'
        yLabel = "1/N dN/(%s) [%s %s]$^{-1}$" % (varNameInYLabel, binSizeStr, units)
        (yMin, yMax) = plt.gca().get_ylim()
        ax.set_ylim((0, yMax*1.5))
        ax.yaxis.set_label_coords(-0.15, 0.75)
        ax.set_ylabel(yLabel)
        ax.set_xlabel(varName+" "+unitStr,horizontalalignment='right', x=1.0)

        leg = plt.legend(loc='best', borderaxespad=0.)
        leg.get_frame().set_linewidth(0.0)
        plt.savefig(varName.replace('[','').replace(']', '')+suffix+'.pdf', bbox_inches='tight')
        if doLog:
            plt.yscale('log', nonposy='clip')
            (yMin, yMax) = plt.gca().get_ylim()
            ax.set_ylim((0, yMax*50))

            plt.savefig(varName.replace('[','').replace(']', '')+suffix+'_log.pdf', bbox_inches='tight')

        plt.close('all')

    
def plotIntermediateLayer(model, data, responses, sigCut, bkgCut, layerInI=0, layerOutI=0, nEvtDisplays=5, suffix=''):
    """Plot the output of a particular layer"""
    sig = data[0]
    bkg = data[1]
    sigResponse = responses[0]
    bkgResponse = responses[1]

    sigPassIndexes = np.where(sigResponse > sigCut)[0]
    bkgPassIndexes = np.where(bkgResponse < bkgCut)[0]
    
    sigRandIndex = [sigPassIndexes[i] for i in np.random.random_integers(0, len(sigPassIndexes), nEvtDisplays)]
    bkgRandIndex = [bkgPassIndexes[i] for i in np.random.random_integers(0, len(bkgPassIndexes), nEvtDisplays)]

    get_layer_output = K.function([model.layers[layerInI].input, K.learning_phase()],
                                  [model.layers[layerOutI].output])
    
    # output in test mode = 0
    sigTestMode = get_layer_output([sig, 0])[0]
    bkgTestMode = get_layer_output([bkg, 0])[0]

    # output in train mode = 1
    sigTrainMode = get_layer_output([sig, 1])[0]
    bkgTrainMode = get_layer_output([bkg, 1])[0]

    # Change this to choose event that are very signal like and very background like 
    for randEvt in sigRandIndex:
        # vmax and vmin is set to +-10 GeV
        plotGrid(sig, randEvt, 'sigEvt_'+str(randEvt), vmax=20000, vmin=-10000, units='$p_T$ [MeV]');
        for filterI in range(sigTestMode[randEvt].shape[0]):
            plotGrid(sigTestMode[:,filterI:filterI+1,:,:], randEvt, 'sigEvt_CNNFilterTestMode'+str(filterI)+suffix+'_'+str(randEvt), vmax=30000, vmin=0, units='AU');
    for randEvt in bkgRandIndex:
        plotGrid(bkgTestMode, randEvt, 'bkgTestEvt_'+str(randEvt), vmax=20000, vmin=-10000, units='$p_T$ [MeV]');
        for filterI in range(bkgTestMode[randEvt].shape[0]):
            plotGrid(bkgTestMode[:,filterI:filterI+1,:,:], randEvt, 'bkgEvt_CNNFilterTestMode'+str(filterI)+suffix+'_'+str(randEvt), vmax=30000, vmin=0, units='AU');
    plt.close('all')



def deepDraw(model, layerI=0, nXBins=14, nYBins=32, suffix=''):
    """ Attempt to draw image from noise based on NN """
    """ Taken from keras example """

    step = 200  # Gradient ascent step size
    iterations = 200  # Number of ascent steps per scale
    
    x = model.layers[layerI].output

    [weights, biases] = model.layers[layerI].get_weights()
    nFilters = weights.shape[-1]
    
    origGrid = np.random.uniform(low=-100, high=100, size=(1, 1, nXBins, nYBins))
    vmax = np.amax(origGrid);
    vmin = np.amin(origGrid);
    newGrids = []
    for filterI in range(nFilters):
         # Create noisy input image:
        grid = np.array(origGrid, copy=True)  
        print "Score for random event before grad ascent", model.predict(grid)

        # Taking the mean output as the loss since we want to maximize this. The sum is another alternative
        # Since the activation is probably ReLU there really isn't a stopping point
        # One possibility is to get the model prediction for each grad step and stop once it is close to 1
        loss = K.mean(x[:,filterI,:,:])

        # Compute the gradients of the dream wrt the loss.
        grads = K.gradients(loss, model.input)[0]

        # Normalize gradients.
        grads /= (K.sqrt(K.mean(K.square(grads))) + 1e-5)

        # Set up function to retrieve the value
        # of the loss and gradients given an input image.
        fetch_loss_and_grads = K.function([model.input], [loss, grads])

        for i in range(iterations):
            loss_value, grads_value = fetch_loss_and_grads([grid])
            # The plus sign makes it a gradient ascent
            grid += grads_value * step

        gridMax = np.amax(grid)
        gridMin = np.amin(grid)
        if gridMax > vmax:
            vmax = gridMax
        if gridMin < vmin:
            vmin = gridMin;
        newGrids.append(np.array(grid, copy=True))
        print "Score for random event after grad ascent", model.predict(grid)

    # Used to up the saturation
    redFrac = 0.7
    plotGrid(origGrid, 0, 'randomInput'+suffix, vmax=vmax*redFrac, vmin=vmin*redFrac, sf=1, units='AU')
    for filterI in range(len(newGrids)):
        grid = newGrids[filterI]
        plotGrid(grid, 0, 'randomInputFilter'+str(filterI)+suffix, vmax=vmax*redFrac, vmin=vmin*redFrac, sf=1, units='AU')

    plt.close('all')

def plotWeights(model, layerI, suffix=''):
    """Plot the weights of a layer"""
    #print model.layers
    [weights, biases] = model.layers[layerI].get_weights()
    #print len(model.layers[-1].get_weights()[0])
    for filterI in range(weights.shape[-1]):
        plt.clf()
        fig, ax = plt.subplots()
        newWeights = np.squeeze(np.squeeze(weights[:,:,:,filterI:filterI+1], axis=3), axis=2)
        print "Sum of all filter elements for filterI=",filterI, round(np.sum(newWeights),2)
        im = plt.imshow(newWeights.T, origin='upper', vmin=-0.5, vmax=0.3)
        cb = fig.colorbar(im)
        #cb.set_label('NN weight ['+unit+']')
        ax.set_xlabel('$\eta$-bin')
        ax.set_ylabel('$\phi$-bin')
        ax.xaxis.set_label_coords(0.9, -0.08)
        ax.yaxis.set_label_coords(-0.25, 0.9)
        for (i,j), x in np.ndenumerate(newWeights.T):
            label = '%.2f' % newWeights[i][j]
            ax.text(i,j,label,ha='center',va='center')

        plt.savefig('weights'+str(filterI)+suffix+'.pdf', bbox_inches='tight')
    [weights, biases] = model.layers[-1].get_weights()
    #print weights
    plt.close('all')
   
def plotFinalWeights(model, convLayerI=0, layerI=-1, suffix=''):
    """Plot the flattened weights of a layer. To do this make a grid again (kind of unflatten the column vector)"""
    #print model.layers
    [weights, biases] = model.layers[layerI].get_weights()
    convOutShape = model.layers[convLayerI].output_shape[1:]
    weights = weights.reshape(1, weights.shape[0])
    weights = weights.reshape(convOutShape)
    for filterI in range(weights.shape[0]):
        plt.clf()
        print "Sum of weights is", round(np.sum(weights[filterI]),2)
        fig, ax = plt.subplots()
        im = plt.imshow(weights[filterI].T, origin='lower', vmin=-0.1, vmax=0.15)
        cb = fig.colorbar(im)
        ax.set_xlabel('$\eta$-bin')
        ax.set_ylabel('$\phi$-bin')
        ax.xaxis.set_label_coords(0.9, -0.08)
        ax.yaxis.set_label_coords(-0.25, 0.9)
        
        plt.savefig('finalWeight'+str(filterI)+suffix+'.pdf', bbox_inches='tight')
    plt.close('all')

def findInputMaxMinAct(model, data, extraVars, extraBranchInfo, layerI, nImages=10):
    """ Find the images with the highest activation for a particular layer. """
    sig = data[0]
    bkg = data[1]

    sigExtraVars = extraVars[0]
    bkgExtraVars = extraVars[1]
    
    [weights, biases] = model.layers[layerI].get_weights()
    nFilters = weights.shape[-1]

    getLayerOutput = K.function([model.layers[layerI].input, K.learning_phase()], [model.layers[layerI].output])
    
    # output in test mode = 0
    sigTestMode = getLayerOutput([sig, 0])[0]
    bkgTestMode = getLayerOutput([bkg, 0])[0]
    
    # output in train mode = 1
    sigTrainMode = getLayerOutput([sig, 1])[0]
    bkgTrainMode = getLayerOutput([bkg, 1])[0]
    maxImages = []

    for filterI in range(nFilters):
        print "\n\nNew filter..."
        sigGridOneFilter = sigTestMode[:,filterI:filterI+1,:,:]
        bkgGridOneFilter = bkgTestMode[:,filterI:filterI+1,:,:]
        sigGridSums = np.average(sigGridOneFilter, axis=(1,2,3))
        bkgGridSums = np.average(bkgGridOneFilter, axis=(1,2,3))
        sortedSigIndexes = np.flip(np.argsort(sigGridSums)[-1*nImages:], 0)
        sortedBkgIndexes = np.flip(np.argsort(bkgGridSums)[-1*nImages:], 0)
        sortedSigIndexesLast = np.flip(np.argsort(sigGridSums)[:1*nImages], 0)
        sortedBkgIndexesLast = np.flip(np.argsort(bkgGridSums)[:1*nImages], 0)
        for i in range(len(sortedSigIndexes)):
            sortedSigIndLast = sortedSigIndexesLast[i]
            sortedBkgIndLast = sortedBkgIndexesLast[i]
            
            sortedSigInd = sortedSigIndexes[i]
            sortedBkgInd = sortedBkgIndexes[i]
            
            print "sig info for max act", filterI, i, sortedSigInd, sigGridSums[sortedSigInd], model.predict(sig[sortedSigInd:sortedSigInd+1])
            print "bkg info for max act", filterI, i, sortedBkgInd, bkgGridSums[sortedBkgInd], model.predict(bkg[sortedBkgInd:sortedBkgInd+1])
            print ""
            print "sig info for min act", filterI, i, sortedSigIndLast, sigGridSums[sortedSigIndLast], model.predict(sig[sortedSigIndLast:sortedSigIndLast+1])
            print "bkg info for min act", filterI, i, sortedBkgIndLast, bkgGridSums[sortedBkgIndLast], model.predict(bkg[sortedBkgIndLast:sortedBkgIndLast+1])
            print "extravars",
            print 
            for varI in range(sigExtraVars.shape[1]):
                scaling = extraBranchInfo[varI][2]
                if not isinstance(extraBranchInfo[varI][0], basestring):
                    varName = extraBranchInfo[varI][0][0]
                else:
                    varName = extraBranchInfo[varI][0]
                outStr = "sigMaxActInfo: {:80} {:>7.1f}".format(varName, round(sigExtraVars[:,varI][sortedSigInd]*scaling,1))
                print outStr
            print ""
            plotGrid(sig, sortedSigInd, 'maxActSigFilter'+str(filterI)+"_"+str(i))
            #plotGrid(sigGridOneFilter, sortedSigInd, 'maxActSigPostFilter'+str(filterI)+"_"+str(i))
            plotGrid(bkg, sortedBkgInd, 'maxActBkgFilter'+str(filterI)+"_"+str(i))
            #plotGrid(bkgGridOneFilter, sortedBkgInd, 'maxActBkgPostFilter'+str(filterI)+"_"+str(i))
            
            plotGrid(sig, sortedSigIndLast, 'minActSigFilter'+str(filterI)+"_"+str(i))
            #plotGrid(sigGridOneFilter, sortedSigIndLast, 'maxActSigPostFilter'+str(filterI)+"_"+str(i))
            plotGrid(bkg, sortedBkgIndLast, 'minActBkgFilter'+str(filterI)+"_"+str(i))
            #plotGrid(bkgGridOneFilter, sortedBkgIndLast, 'maxActBkgPostFilter'+str(filterI)+"_"+str(i))


def getPlaneWeights(var):
    """ Apply planing of interesting variables to see what the NN is learning. More info is here
    https://arxiv.org/abs/1709.10106. Essentially you calculate weights to flatten the distribution (no info) for a 
    particular variable (say leading jet pT) and retrain with the new weights. How to implement this for a CNN?"""


def makePseudoData(model, nXBins, nYBins, layerInI=0, layerOutI=0):
    """ Study the effect a model has on simple pseudo data (drawing blocks or circles). """
    minVal = -1000
    maxVal = 9000
    origGrid = np.random.uniform(low=minVal, high=maxVal, size=(1, 1, nXBins, nYBins))

    plotGrid(origGrid, 0, 'origUniform')
    origResponse = model.predict(origGrid)
    print origGrid.shape, origResponse
    squareGrid = np.random.uniform(low=minVal, high=maxVal, size=(1, 1, nXBins, nYBins))
    squareGrid[0,0,14:19,5:10] = 20000
    #squareGrid[0,0,7:12,15:20] = 10000
    print model.predict(squareGrid)
    plotGrid(squareGrid, 0, 'squareUniform')

    get_layer_output = K.function([model.layers[layerInI].input, K.learning_phase()],
                                  [model.layers[layerOutI].output])
    
    # output in test mode = 0
    origTestMode = get_layer_output([origGrid, 0])[0]
    squareTestMode = get_layer_output([squareGrid, 0])[0]

    # output in train mode = 1
    origTrainMode = get_layer_output([origGrid, 1])[0]
    squareTrainMode = get_layer_output([squareGrid, 1])[0]

    for filterI in range(squareTestMode[0].shape[0]):
        plotGrid(squareTestMode[:,filterI:filterI+1,:,:], 0, 'square_CNNFilterTestMode'+str(filterI), units='AU');
   
    plt.close('all')

def checkEventOverlap():
    """ Check the event overlap of a simple 3 seed requirement and the NN. """


def plotVarsLowResponse():
    """ Plot variables for events that have a low NN response."""

    
def plotSignifVsEff(precutSigYield, precutBkgYield, sigEffs, bkgEffs, db=0.3, suffix='', mvcOutputRange=[0,1], desiredSigEff=1):
    """Plot the binomobsZ vs signal efficiency"""
    fig, ax = plt.subplots()

    sigYields = {}
    bkgYields = {}
    
    sigPlusBkgYields = {}
    binomObsZ = {}
    signifs = {}
    for modelKey in sorted(sigEffs.keys()):
        bkgYields[modelKey] = [bkgEff*precutBkgYield for bkgEff in bkgEffs[modelKey]]
        sigYields[modelKey] = [sigEff*precutSigYield for sigEff in sigEffs[modelKey]]
        sigPlusBkgYields[modelKey] = [sigYields[modelKey][i]+bkgYields[modelKey][i] for i in range(len(bkgYields[modelKey]))]
        binomObsZ[modelKey] = [RooStats.NumberCountingUtils.BinomialObsZ(sigPlusBkgYields[modelKey][i], bkgYields[modelKey][i], db) for i in range(len(bkgYields[modelKey]))]
        signifs[modelKey] = [(sigYields[modelKey][i])/(np.sqrt(sigPlusBkgYields[modelKey][i]+(bkgYields[modelKey][i]*db)**2)) for i in range(len(bkgYields[modelKey]))]

    # Let's plot the significance as a function of signal efficiency
    for modelKey in sorted(sigEffs.keys()):
        if len(binomObsZ[modelKey]) > 1:
            plt.plot(sigEffs[modelKey], binomObsZ[modelKey], label=modelKey.replace('_model_', '_')+suffix)
        else:
            plt.plot(sigEffs[modelKey], binomObsZ[modelKey], 's', label=modelKey.replace('_model_', '_')+suffix)
            
    ax.set_xlabel('Signal efficiency')
    ax.set_ylabel('$\sigma$')
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)

    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.4))
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    #plt.savefig('obZVsSigEff'+suffix+'.pdf', bbox_inches='tight')

    plt.clf()
    fig, ax = plt.subplots()
    for label in sorted(sigEffs.keys()):
        if len(signifs[label]) > 1:
            plt.plot(sigEffs[label], signifs[label], label=label.replace('_model_', '_')+suffix)
        else:
            plt.plot(sigEffs[label], signifs[label], 's', label=label.replace('_model_', '_')+suffix)
    ax.set_xlabel('Signal efficiency')
    ax.set_ylabel('S/$\sqrt{S+B+\Delta B^2}$' )
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)

    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.6))
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    plt.tight_layout()
    plt.savefig('signifVsSigEff'+suffix+'.pdf', bbox_inches='tight')


    # Now let's plot the background yields as a function of signal efficiency.
    plt.clf()
    fig, ax = plt.subplots()

    for label in sorted(sigEffs.keys()):
        if len(sigYields[label]) > 1:
            plt.semilogy(sigEffs[label], bkgYields[label], label=label.replace('_model_', '_'))
        else:
            plt.semilogy(sigEffs[label], bkgYields[label], 's', label=label.replace('_model_', '_'))
    ax.set_xlabel('Signal efficiency')
    ax.set_ylabel('Bkg yield' )
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)

    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.4))
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    
    plt.savefig('yieldSigEff'+suffix+'.pdf', bbox_inches='tight')

    # Plot the signal efficiency as a function of multivariate classifier output.
    plt.clf()
    fig, ax = plt.subplots()
    mvcCuts = {}
    for label in sorted(sigEffs.keys()):
        mvcOutput = [i*(mvcOutputRange[1]*1.0/len(sigEffs[label]))+mvcOutputRange[0] for i in range(len(sigEffs[label]))]
        plt.plot(sigEffs[label], mvcOutput, label=label.replace('_model_', '_'))
        desiredSigEffIndex = min(range(len(sigEffs[label])), key=lambda i: abs(sigEffs[label][i]-desiredSigEff))
        desiredMVCOutput = mvcOutput[desiredSigEffIndex]
        print "Best MVC output cut for", label, "=", desiredMVCOutput
        mvcCuts[label] = desiredMVCOutput
        
    ax.set_ylabel('Signal efficiency')
    ax.set_xlabel('MVC output' )
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)

    (yMin, yMax) = plt.gca().get_ylim()
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    
    plt.savefig('sigEffs'+suffix+'.pdf', bbox_inches='tight')

    # Plot the background efficiency as a function of multivariate classifier output.
    plt.clf()
    fig, ax = plt.subplots()

    for label in sorted(bkgEffs.keys()):
        mvcOutput = [i*(mvcOutputRange[1]*1.0/len(bkgEffs[label]))+mvcOutputRange[0] for i in range(len(bkgEffs[label]))]
        plt.plot(bkgEffs[label], mvcOutput, label=label.replace('_model_', '_'))
    ax.set_ylabel('Background efficiency')
    ax.set_xlabel('MVC output' )
    ax.xaxis.set_label_coords(0.9, -0.08)
    ax.yaxis.set_label_coords(-0.1, 0.6)

    (yMin, yMax) = plt.gca().get_ylim()
    leg = plt.legend(loc=0, borderaxespad=0.)
    leg.get_frame().set_linewidth(0.0)
    
    plt.savefig('bkgEffs'+suffix+'.pdf', bbox_inches='tight')
    plt.close('all')

    return mvcCuts


def analyzeTrees(xgbClass, featureNames, binning, suffix=''):
    """! This function will analyze trees. """
    booster = xgbClass.get_booster()
    booster.dump_model('trees'+suffix+'.txt')

    trees = booster.get_dump(with_stats=True)
    print trees[0]
    print len(trees)

    # Plot the importance of various inputs.
    fig, ax = plt.subplots()
    sortedImportance = sorted(xgbClass.feature_importances_, reverse=True)
    sortedFeatureNames = [fName for imp,fName in sorted(zip(xgbClass.feature_importances_,featureNames), reverse=True)]
    plt.bar(range(len(xgbClass.feature_importances_)), sortedImportance)
    ax.set_xticklabels(['']+sortedFeatureNames,rotation=45, rotation_mode="anchor", ha="right")
    plt.ylabel('input variable importance (weight)')
    plt.savefig('importance_weight'+suffix+'.pdf', bbox_inches='tight')

    fig, ax = plt.subplots()
    tempImp = booster.get_score(importance_type='cover')
    importance = [tempImp[feature] for feature in sorted(tempImp.keys(), key=lambda featKey: int(featKey[1:]))]
    sortedImportance = sorted(importance, reverse=True)
    sortedFeatureNames = [fName for imp,fName in sorted(zip(importance,featureNames), reverse=True)]
    plt.bar(range(len(importance)), sortedImportance)
    ax.set_xticklabels(['']+sortedFeatureNames,rotation=45, rotation_mode="anchor", ha="right")
    plt.ylabel('input variable importance (cover)')
    plt.savefig('importance_cover'+suffix+'.pdf', bbox_inches='tight')

    fig, ax = plt.subplots()
    tempImp = booster.get_score(importance_type='gain')
    importance = [tempImp[feature] for feature in sorted(tempImp.keys(), key=lambda featKey: int(featKey[1:]))]
    sortedImportance = sorted(importance, reverse=True)
    sortedFeatureNames = [fName for imp,fName in sorted(zip(importance,featureNames), reverse=True)]
    plt.bar(range(len(importance)), sortedImportance)
    ax.set_xticklabels(['']+sortedFeatureNames,rotation=45, rotation_mode="anchor", ha="right")
    plt.ylabel('input variable importance (gain)')
    plt.savefig('importance_gain'+suffix+'.pdf', bbox_inches='tight')

    # Plot the negative likelihood function and classification error as a function of trees.
    plt.clf()
    fig, ax = plt.subplots()
    results = xgbClass.evals_result()
    epochs = len(results['validation_0']['error'])
    x_axis = range(0, epochs)
    ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
    ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.2))
    ax.legend()
    plt.ylabel('Log-likelihood')
    plt.xlabel('Number of trees')
    plt.savefig('logLoss'+suffix+'.pdf', bbox_inches='tight')

    plt.clf()
    fig, ax = plt.subplots()
    results = xgbClass.evals_result()
    epochs = len(results['validation_0']['error'])
    x_axis = range(0, epochs)
    ax.plot(x_axis, results['validation_0']['error'], label='Train')
    ax.plot(x_axis, results['validation_1']['error'], label='Test')
    ax.legend()
    (yMin, yMax) = plt.gca().get_ylim()
    ax.set_ylim((0, yMax*1.2))
    plt.ylabel('Classification error')
    plt.xlabel('Number of trees')
    plt.savefig('error'+suffix+'.pdf', bbox_inches='tight')
  
    for featureI in range(len(featureNames)):
        splitVals = booster.get_split_value_histogram('f'+str(featureI),as_pandas=False)
        newSplitVals = []
        for val in splitVals:
            for i in range(int(val[1])):
                newSplitVals.append(val[0])
        # Plot all trees first
        plt.clf()
        fig, ax = plt.subplots()
        plt.hist(np.clip(newSplitVals, binning[featureNames[featureI]][0][0], binning[featureNames[featureI]][0][-1]), bins=binning[featureNames[featureI]][0], histtype='step')
        plt.xlabel(featureNames[featureI]+" cut value",horizontalalignment='right', x=1.0)
    
        yLabel = "Frequency"
        plt.ylabel(yLabel)
        plt.savefig("CutValue_"+featureNames[featureI].replace('[','').replace(']', '')+suffix+'.pdf', bbox_inches='tight')
